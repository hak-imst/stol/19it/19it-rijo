## A)	Der bereits per Mail zugestellte Messe-Guide gibt dir einen Überblick über die ausstellenden Betriebe. Plane den Besuch im Vorhinein.


Ich werde beim Karrieretag zu folgenden Betriebe gehen:
Acp, Weco, Hofer, Mpreis, SpeedyouUp, Translogica

folgende möchte ich auch noch schaffen aber da mein Bus um 15:45 kommt habe ich leider nur Zeit bis 15:30:
Conform, Egger


## B)	Suche dir möglichst viele Betriebe aus, die dich besonders interessieren und komme mit den Vertretern der Unternehmen über folgende Fragen ins Gespräch:

# Acp: 
rund um Support Kundenservice und Cloud Systeme
alles auf Windows Server dadurch besteht wenig Risiko für Datenverlust bei einer Kauputtenfestplatte etc.
marktführer in Österreich
70 Mitarbeiter in Innsbruck
Praktikum möglich, Diplomarbeit nicht

# Translogica:
softwareentwickler für Transport und Datenbankprozesse
abwicklung von transportprozessen und planung in 2D für realisierungen mit virtual reality brille auch in 3D
von hardcore programmierung bis zu programmplanung
nur 4 praktika pro jahr, habe mich angemeldet
diplomarbeit möglich

# Weco:
Website/Webshop mit Sap
javascript und sap wichtig
sap und engluar wird verwendet
Vorteile: Preise werden durch Sap geupdated, alles wird direkt in Sap gebucht und auch abgewickelt
Praktikum melden anfang jänner (angemeldet)
Diplomarbeit eventuell mit einem frontend template machen aber eher schwierig


# Hofer:
Projektabwicklung in Rietz
Dev Team in Oberösterreich
Javascript aber immer mehr Sap
national weite lösungen
Offene Stellen auf der Website

# Mpreis:
Diplomarbeit möglich
Praktikum ab anfang jänner bewerben
Aufstiegsmöglichkeiten Team-Teamleiter
Hirachie aber eher flach in der IT

# Egger:
3 Abteilungen:
Klassische Softwareentwicklung

SS Abteilung
Maschinen Informationaustausch

Datenbanken
Aufgrund daten des Kaufmönnischen bereichs in die Datenbank damit das management das verarbeitet



# Conform:
Datenerfassungsteam
Produktion und Vertrieb
Praktikum möglich
Diplomarbeit möglich
80 Mitarbeiter davon in der It 7


## C) In / mit welchen der besuchten Betriebe könntest du dir sehr gut vorstellen …
Ich könnte mir weco gut vorstellen da es nicht so weit entfernt ist und auch sehr interessant ist da ich gut in sap bin. Leider ist es etwas kompliziert daher wird ein Praktikum schwierig.

Translogica ist ein sehr großer Betrieb allerdings gibt es nur 4 Praktikumstellen daher wird das sehr schwer ein Praktikum zu bekommen.

Egger könnte ich mir nicht vorstellen da es in St Johann ist.
